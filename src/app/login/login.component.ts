import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = {};
  loading: boolean = false;
  constructor( private loginService: LoginService) { }
  ngOnInit() {}

  login(): void{
    this.loading = true;
    if(this.user){
    this.loginService.login(this.user).then(currentUser =>{
      this.loading = false;
      console.log(currentUser);
    }).catch(error => {
        console.log(error);
        this.loading = false;
      })
    }
  }
}
