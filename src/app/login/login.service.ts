import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {
  private url = "https://forte.medplan.com.br/integrationservices/auth/login";
  constructor(private http: Http) {  }

  login(user): Promise<any>{
    return new Promise((resolve, reject) => {
      this.http.post(this.url, user)
      .map(data => data.json())
      .subscribe(data =>{
        if(data.status === 401){
          reject("Usuário ou senha inválidos");
        }else{
          resolve(data);
        }
      });
    });
  }
}
