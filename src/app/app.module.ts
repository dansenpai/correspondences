import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Md2Module } from 'md2';
import { NgxDatatableModule  } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { CorrespondenceComponent } from './correspondence/correspondence.component';
import { NavbarComponent } from './navbar/navbar.component';

import { CorrespondenceService } from './correspondence/correspondence.service';
import { DeliveryComponent } from './delivery/delivery.component';
import { ReportComponent } from './report/report.component';
import { PdfviewComponent } from './pdfview/pdfview.component';
import { PdfviewService } from './pdfview/pdfview.service';
import { LoginService } from './login/login.service';

import {MomentModule} from 'angular2-moment';
import { LoginComponent } from './login/login.component';
import { TemplateNotFoundComponent } from './template-not-found/template-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    CorrespondenceComponent,
    NavbarComponent,
    DeliveryComponent,
    ReportComponent,
    PdfviewComponent,
    LoginComponent,
    TemplateNotFoundComponent
  ],
  imports: [
    BrowserModule,
    NgxDatatableModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    MaterialModule.forRoot(),
    FlexLayoutModule.forRoot(),
    Md2Module.forRoot(),
    MomentModule
  ],
  providers: [ CorrespondenceService , PdfviewService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
