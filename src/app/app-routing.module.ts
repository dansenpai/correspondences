import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CorrespondenceComponent } from './correspondence/correspondence.component'
import { DeliveryComponent } from './delivery/delivery.component';
import { ReportComponent } from './report/report.component';
import { PdfviewComponent } from './pdfview/pdfview.component';
import { LoginComponent } from './login/login.component';
import { TemplateNotFoundComponent } from './template-not-found/template-not-found.component';

const routes: Routes = [
  { path: 'correspondencias', component: CorrespondenceComponent },
  { path: 'entrega', component: DeliveryComponent },
  { path: 'relatorio', component: ReportComponent },
  { path: 'pdf', component: PdfviewComponent },
  { path: 'login', component: LoginComponent },
  { path: 'notfound', component:  TemplateNotFoundComponent },
  { path: '**', redirectTo: '/notfound', pathMatch: 'full'}
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}