import { Component, OnInit } from '@angular/core';
import { CorrespondenceService } from '../correspondence/correspondence.service';
import { Correspondence } from '../correspondence/correspondence';
import { PdfviewComponent } from "../pdfview/pdfview.component";
import { Router } from "@angular/router";
import { PdfviewService } from '../pdfview/pdfview.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  constructor(private location: Location, private router: Router,  private correspondenceService: CorrespondenceService, private pdfviewService: PdfviewService) { };
  correspondences: Correspondence[];
  selected: Correspondence[];
  filters: Array<Object>;
  // Message for table, when no data and data total
  messages = {
    emptyMessage: "Não há comprovantes a serem impressos",
    totalMessage: "total"
  }

  ngOnInit() {
    this.filters = [
      {name: "Todas", value: true},
      {name: "Entregues", value: true},
      {name: "Não entregues", value: false}
    ];
    this.selected = [];
    this.getCorrespondencies();
  }

  getCorrespondencies(): void {
    this.correspondences = [];
    this.correspondenceService.getCorrespondencies().then(correspondences =>{
      correspondences.forEach(correspondence => {
        if(correspondence.delivered) this.correspondences.push(correspondence);
      });
    });
  }

  getRelatorio(correspondence?): void{
    if(correspondence) this.selected.push(correspondence);
    if(this.selected.length > 0) {
      this.selected.forEach(correspondence => {
        correspondence.printed = true;
        this.correspondenceService.updateCorrespondence(correspondence)
        .then(() => {
          if(this.selected.length > 0 ) {
            this.pdfviewService.setSelected(this.selected).then(()=> {
              this.router.navigate(['/pdf']);
            });
          }
        });
      });
    }
  }

  goBack(): void {
    this.location.back();
  }
}
