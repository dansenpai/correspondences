export class Correspondence {
  _id: number;
  date: string | Date;
  sender: string;
  receiver: string;
  note: string;
  delivered: boolean = false;
  printed: boolean = false;
}