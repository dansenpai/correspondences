import { Component, OnInit } from '@angular/core';
import { CorrespondenceService } from './correspondence.service';
import { Correspondence } from './correspondence';

@Component({
  selector: 'app-correspondence',
  templateUrl: './correspondence.component.html',
  styleUrls: ['./correspondence.component.scss']
})
export class CorrespondenceComponent implements OnInit {
  model: Correspondence = new Correspondence();
  loading: boolean;

  constructor(private correspondenceService: CorrespondenceService) { }
  ngOnInit() {
  }

  createCorrespondence(): void{
    // add form validation
    this.correspondenceService.createCorrespondence(this.model).then(() =>{
      this.model = new Correspondence();
    })
  }

  getCorrespondences(): Correspondence[]{
    return [];
  }

  getCorrespondence(id: number): Correspondence{
    return  ;
  }

  formValid(): boolean{
    if(this.model.date && this.model.sender && this.model.receiver){
      return true;
    }
    return false;
  }
}
