/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CorrespondenceService } from './correspondence.service';

describe('CorrespondenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CorrespondenceService]
    });
  });

  it('should ...', inject([CorrespondenceService], (service: CorrespondenceService) => {
    expect(service).toBeTruthy();
  }));
});
