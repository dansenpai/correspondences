import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Correspondence } from './correspondence';
import {MdSnackBar} from '@angular/material';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CorrespondenceService {
  private server = "http://localhost:3000";
  
  constructor(public snackBar: MdSnackBar, private http: Http) { }

  getCorrespondencies(): Promise<any>{
    return new Promise(resolve => {
      this.http.get(this.server + "/correspondencies")
      .map(data => data.json())
      .subscribe(data =>{
        resolve(data);
      });
    });
  }

  deliveryCorrespondencies(correspondence: Correspondence): Promise<any> {
    return new Promise(resolve => {
      correspondence.delivered = true;
      this.updateCorrespondence(correspondence).then(() => {
        this.openSnackBar("Correspondência entregue");
        resolve();
      });
    });
  }

  updateCorrespondence(correspondence: Correspondence): Promise<any>{
    return new Promise(resolve => {
      this.http.put(this.server + "/correspondencies/" + correspondence._id, correspondence)
      .map(data => data.json())
      .subscribe(data => {
        resolve();
      });
    });
  }

  createCorrespondence(correspondence): Promise<any>{
    return new Promise(resolve => {
      this.http.post(this.server + "/correspondencies", correspondence)
      .map(data => data.json())
      .subscribe(data => {
        this.openSnackBar("Correspondência cadastrada");
        resolve();
      });
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message,"", {
      duration: 5000
    });
  }
}
