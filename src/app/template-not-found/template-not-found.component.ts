import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-template-not-found',
  templateUrl: './template-not-found.component.html',
  styleUrls: ['./template-not-found.component.scss']
})
export class TemplateNotFoundComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }
  backHome(): void{
    this.location.back();
  }

}
