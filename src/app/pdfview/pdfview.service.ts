import { Injectable } from '@angular/core';
import { Correspondence } from '../correspondence/correspondence';

@Injectable()
export class PdfviewService {
  constructor() { }
  selected: Correspondence[] =[];

  getSelected(): Promise<Correspondence[] >{
    return Promise.resolve(this.selected);
  }

  setSelected(selected): Promise<void>{
    this.selected = selected;
    return Promise.resolve();
  }
}
