import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PdfviewService } from '../pdfview/pdfview.service';
import { Correspondence } from '../correspondence/correspondence';

let date = Date.now();

@Component({
  selector: 'app-pdfview',
  templateUrl: './pdfview.component.html',
  styleUrls: ['./pdfview.component.scss']
})
export class PdfviewComponent implements OnInit {
  correspondences: Correspondence[] = [];
  constructor( private pdf: PdfviewService ) { }
  ngOnInit() {
    this.pdf.getSelected().then(correspondences => {
      this.correspondences = correspondences;
    });
  }

  print(): void{
    window.print();
  }
}
