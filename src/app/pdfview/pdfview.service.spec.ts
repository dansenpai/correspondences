/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PdfviewService } from './pdfview.service';

describe('PdfviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PdfviewService]
    });
  });

  it('should ...', inject([PdfviewService], (service: PdfviewService) => {
    expect(service).toBeTruthy();
  }));
});
