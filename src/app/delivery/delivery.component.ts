import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Correspondence } from '../correspondence/correspondence';
import { CorrespondenceService } from '../correspondence/correspondence.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
  constructor(private location: Location, private correspondenceService: CorrespondenceService) { };

  correspondences: Correspondence[];
  selected: Correspondence[];

  messages = {
    emptyMessage: "Nenhuma correspondência a ser entregue",
    totalMessage: "total"
  }

  ngOnInit() {
    this.selected = [];
    this.correspondences = [];
    this.getCorrespondencies();
  }

  getCorrespondencies(): void{
    let array = [];
    this.correspondenceService.getCorrespondencies().then(correspondences => {
      correspondences.forEach(correspondence => {
          if(!correspondence.delivered) array.push(correspondence);
      });
      this.correspondences = array;
    });
  }

  deliveryCorrespondencies(correspondence?): void{
    if(correspondence)(this.selected.push(correspondence));
    if(this.selected.length > 0){
      this.selected.forEach(correspondence => {
        this.correspondenceService.deliveryCorrespondencies(correspondence).then(()=> {
          this.selected = [];
          this.getCorrespondencies();
        });
      });
     }
  }
  goBack(): void {
    this.location.back();
  }
}