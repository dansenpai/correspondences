import { MedprotocoloPage } from './app.po';

describe('medprotocolo App', function() {
  let page: MedprotocoloPage;

  beforeEach(() => {
    page = new MedprotocoloPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
